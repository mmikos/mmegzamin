__author__ = 'ryujin'
import json
from django.views.decorators.csrf import csrf_exempt
import spider
from django.http import HttpResponse, Http404
from django.template.response import TemplateResponse
from django.core.context_processors import csrf
from forms import *
import models
from beaker import *

__author__ = 'ryujin'


def hello(request):
    spider.Spider().run()
    return TemplateResponse(request, 'calculate.html')



def result(request):
    return TemplateResponse(request,'result.html',
                            {'blog_entries': "", 'information': models.newsModel.objects.all()})

def register(request):
    c = {}
    f = []
    c.update(csrf(request))
    if request.method == "GET":

        a = RegisterForm(initial={'login': 'sdfsdfs'});

        return TemplateResponse(request, 'register.html', {'form': a, 'c': c})
    elif request.method == "POST":

        return TemplateResponse(request, 'register.html', {'authenticated': "false", 'c': c})


def search(request):
    if request.method == 'POST':
        results = []
        keyArray=request.POST['q']
        for item in models.newsModel.objects.all():
            for key in keyArray:
                if key in item.description:
                    results.append(key)

        return TemplateResponse(request, 'result.html', {'results': results})

    return TemplateResponse(request, 'error.html', {'results': ''})


@csrf_exempt
def SendCalculate(request):
    if request.method == "POST":
        try:
            jsonData = json.loads(request.body)
            liczba1 = float(jsonData['number1'])
            liczba2 = float(jsonData['number2'])
            wynik = float(liczba1 + liczba2)
            return HttpResponse(json.dumps(wynik), content_type="application/json")
        except:
            return Http404()
    else:
        return Http404()

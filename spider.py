__author__ = 'ryujin'
import urllib
from bs4 import BeautifulSoup
from models import newsModel
from django.db import models

sites = ['http://wp.pl','http://onet.pl','http://interia.pl']
class Spider:
    def run(self):
        for site in sites:
            data = urllib.urlopen(site)
            loadedData = BeautifulSoup(data)
            for link in loadedData.find_all('a'):
                news = newsModel.objects.create(link=link.get('href'), description=link.get('title'))
                if news not in models.newsModel.objects.all():
                    news.save()







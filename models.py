__author__ = 'ryujin'
from django.db import models
from django.contrib.auth.models import User

class registerModel(models.Model):
    host = models.TextField()
    active=models.BooleanField()
    operator=models.CharField(max_length=1)
    calculation=models.TextField()

class newsModel(models.Model):
    link = models.TextField()
    description= models.TextField()

